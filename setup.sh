#git clone https://gitlab.com/farukshin/nginx-gunicorn-flask-install.git

sudo apt-get update
sudo apt-get -y install python3-pip nginx git mc supervisor

sudo -H pip3 install --upgrade pip
sudo pip3 install -r ~/nginx-gunicorn-flask-install/requirements.txt

sudo cp ~/nginx-gunicorn-flask-install/supervisor.conf /etc/supervisor/conf.d/supervisor.conf
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start test_project

sudo service nginx stop
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/default
sudo cp ~/nginx-gunicorn-flask-install/nginx-conf.conf /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/nginx-conf.conf /etc/nginx/sites-enabled/
sudo nginx -t
sudo service nginx start